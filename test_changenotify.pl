#! perl -slw
use strict;
use Win32::ChangeNotify;

our $PATH ||= '.';
our $S = defined $S ? 1 : 0;

$PATH = 'C:/slidefish/input',

my $notify = Win32::ChangeNotify->new( $PATH, $S, 'FILE_NAME' );

my %last; @last{ glob $PATH . '/*' } = ();

while( 1 ) {
    print('Nothing changed'), next
        unless $notify->wait( 10_000 ); # Check every 10 seconds
    $notify->reset;
    print 'Something changed';
    my @files = glob $PATH . '/*';
    if( @files < scalar keys %last ) {
        delete @last{ @files };
        print 'These files where deleted: ';
        print for keys %last;
    }
    elsif( @files > scalar keys %last ) {
        my %temp;
        @temp{ @files } = ();
        delete @temp{ keys %last };
        print 'These files where created: ';
        print for keys %temp;
    }
    else {
        print "A non-deletion or creation change occured";
    }
    undef %last;
    @last{ @files } = ();
}
