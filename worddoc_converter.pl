#
# C:\slidefish\worddoc_converter.pl --test --debug
# --indir C:\slidefish\input
# --outdir C:\slidefish\output
# --infilen AKCChart0607doc.doc
# --pdf AKCChart0607doc.pdf 

use strict; 
use warnings;
no warnings qw(once);
use Try::Tiny;
use Getopt::Long;

require './perl_DB/db_log_DBI.pl';

my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->debug('worddoc_converter Logging initialised, yay!');
$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL
$log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL

use Win32::OLE qw(in with);
use Win32::OLE::Const qw( Microsoft.Word );
use Win32::OLE::Const "Microsoft Office 15.0 Object Library";  # mso constants

use Win32::OLE::Enum;
$Win32::OLE::Warn = 3;

my $ppc = Win32::OLE::Const->Load(qw( Microsoft.Word ));

my ($wd_input_dir, $wd_output_dir, $wd_output_file, $test, $debug);
my ($input_fn, $out_pdf_fn);
my ($input_file, $pdf_output_file);
$wd_input_dir = $wd_output_dir = $input_fn = '';
$wd_output_file = '';
$out_pdf_fn = '';
$input_file = $pdf_output_file = '';
$test = $debug = 0;


if($test) {

	# $log_f->debug('worddoc_converter: using test variables');
	$wd_input_dir = qw( C:/slidefish/processing );  
	$wd_output_dir = qw( C:/slidefish/output ); 
	$input_fn = "AKCChart0607doc.doc";
	$out_pdf_fn = "AKCChart0607doc.doc.pdf";
	
 }
 
  GetOptions (
			'test!' => \$test,
			'debug!' => \$debug,
			'indir=s' => \$wd_input_dir,
			'outdir=s' => \$wd_output_dir,
			'infilen=s' => \$input_fn,
			'pdf=s' => \$out_pdf_fn);
			
$wd_input_dir =~ tr#\\#/#;
$wd_output_dir =~ tr#\\#/#;
$input_file = $wd_input_dir . "/" . $input_fn;
$pdf_output_file = $wd_output_dir . "/" . $out_pdf_fn;
$wd_output_file = $wd_output_dir . "/" . $input_fn;

$debug && $log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL

$log_f->debug( "worddoc_converter: inputdir: (" . $wd_input_dir . "), ouputdir: (" . $wd_output_dir  . ")");

(-e $wd_input_dir) || 
	$log_f->logdie("INPUT DIRECTORY FAILURE: $wd_input_dir!");
(-e $wd_output_dir) || 
	$log_f->logdie("OUTPUT DIRECTORY FAILURE: $wd_output_dir!");

$log_f->debug('worddoc_converter: Input file: (' . $input_file  . ')');

my($wd, $wdoc);
$wd = get_Word();
#$wd->{Visible} = 1;
$test && ($wd->{Visible} = 1);

# $input_file =~ tr#/#\\#;
$log_f->debug('worddoc_converter: opening Input file: (' . $input_file . ')');
$log_f->debug('worddoc_converter: length Input file name: (' . length($input_file) . ')');
 
 # open the workbook
(-e $input_file) ||
	$log_f->logcroak(sprintf("worddoc_converter: input file (%s) does not exist", $input_file));
$input_file =~ tr#/#\\#;
 $wdoc = $wd->Documents->Open($input_file);
$input_file =~ tr#\\#/#;

$wdoc || $log_f->logdie( 'worddoc_converter: NOCATCH: workbook loading Error');

#$debug && print 'saving pdf file: (' . $pdf_output_file . ")\n";
$log_f->debug( 'worddoc_converter: saving pdf file: (' . $pdf_output_file . ')');

# (-e $wd_output_dir) || 
 # die "\noutput directory does not exist: (" . $pdf_output_file . ")\n";

$pdf_output_file =~ tr#/#\\#;

$wdoc->ExportAsFixedFormat( {
			OutputFileName =>  $pdf_output_file, 
			ExportFormat =>  wdExportFormatPDF, 
			OpenAfterExport => 'False', 
			OptimizeFor => wdExportOptimizeForOnScreen, 
			Range => wdExportAllDocument,
			Item => wdExportDocumentContent, 
			IncludeDocProps => 'False', 
			KeepIRM => 'False',
			CreateBookmarks => wdExportCreateNoBookmarks, 
			DocStructureTags => 'False',
			BitmapMissingFonts => 'True', 
			UseISO19005_1 => 'True'
		} );
$wdoc->Close;
$pdf_output_file =~ tr#\\#/#;
$log_f->info( 'worddoc_converter: (' . $pdf_output_file . ') export completed');


# move the input file to the output directory
rename($input_file, $wd_output_file) || die "$0: unable to move " . $input_file . " to " . $wd_output_file;
$log_f->info( 'worddoc_converter: (' . $wd_output_file . ') processing completed');

if ($debug) {
	use File::Copy;
	$log_f->debug( 'worddoc_converter: copy source (' . $wd_output_file . ') to ' . $input_file);
	copy ($wd_output_file, $input_file);
}	

#$wd->Exit;


sub get_Word {
    my $wd;

    try {
        $wd = Win32::OLE->GetActiveObject('Word.Application');
		if($wd) {
			while(($wd) && (defined ($wd->ActiveDocument))) { #it is in use
				sleep(1);
			}
		}
    }
    catch {
        die $_;
    };

    unless ( $wd ) {
        $wd = Win32::OLE->new('Word.Application',  sub { $_[0]->Quit }
        ) or (
			$log_f->logcroak('Cannot start Word: %s', Win32::OLE->LastError)
		);
    }

	#$debug && (print "get_Word: \$wd = $wd\n");
	$log_f->debug( "worddoc_converter: get_Word: $wd = (" . $wd . ")");

	$wd->{DisplayAlerts}=0; 
    return $wd;
}