#
# C:\slidefish\xl_converter.pl --test --debug
# --indir C:\slidefish\input
# --outdir C:\slidefish\output
# --infilen Title1.xls
# --pdf title1.pdf 

use strict; 
use warnings;
no warnings qw(once);
use Try::Tiny;
use Getopt::Long;

require './perl_DB/db_log_DBI.pl';

my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->debug('xl_converter Logging initialised, yay!');
$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL
# $log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL

use Win32::OLE qw(in with);
use Win32::OLE::Const qw( Microsoft.Excel );
use Win32::OLE::Const "Microsoft Office 15.0 Object Library";  # mso constants

use Win32::OLE::Enum;
$Win32::OLE::Warn = 3;

my $ppc = Win32::OLE::Const->Load(qw( Microsoft.Excel ));

my ($xl_input_dir, $xl_output_dir, $xl_output_file, $test, $debug);
my ($input_fn, $out_pdf_fn);
my ($input_file, $pdf_output_file);
$xl_input_dir = $xl_output_dir = $input_fn = '';
$xl_output_file = '';
$out_pdf_fn = '';
$input_file = $pdf_output_file = '';
$test = $debug = 0;


if($test) {

	# $log_f->debug('xl_converter: using test variables');
	$xl_input_dir = qw( C:/slidefish/processing );  
	$xl_output_dir = qw( C:/slidefish/output ); 
	$input_fn = "AKCChart 0607xls.xls";
	$out_pdf_fn = "AKCChart 0607xls.xls.pdf";
	
 }
 
  GetOptions (
			'test!' => \$test,
			'debug!' => \$debug,
			'indir=s' => \$xl_input_dir,
			'outdir=s' => \$xl_output_dir,
			'infilen=s' => \$input_fn,
			'pdf=s' => \$out_pdf_fn);
			
$xl_input_dir =~ tr#\\#/#;
$xl_output_dir =~ tr#\\#/#;
$input_file = $xl_input_dir . "/" . $input_fn;
$pdf_output_file = $xl_output_dir . "/" . $out_pdf_fn;
$xl_output_file = $xl_output_dir . "/" . $input_fn;

$debug && $log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL

$log_f->debug( "xl_converter: inputdir: (" . $xl_input_dir . "), ouputdir: (" . $xl_output_dir  . ")");

(-e $xl_input_dir) || 
	$log_f->logdie("INPUT DIRECTORY FAILURE: $xl_input_dir!");
(-e $xl_output_dir) || 
	$log_f->logdie("OUTPUT DIRECTORY FAILURE: $xl_output_dir!");

$log_f->debug('xl_converter: Input file: (' . $input_file  . ')');

my($xls, $workbook);
$xls = get_excel();
#$xls->{Visible} = 1;
$test && ($xls->{Visible} = 1);

# $input_file =~ tr#/#\\#;
$log_f->debug('xl_converter: opening Input file: (' . $input_file . ')');
$log_f->debug('xl_converter: length Input file name: (' . length($input_file) . ')');
 
 # open the workbook
(-e $input_file) ||
	$log_f->logcroak(sprintf("xl_converter: input file (%s) does not exist", $input_file));
$input_file =~ tr#/#\\#;
 $workbook = $xls->Workbooks->Open($input_file);
$input_file =~ tr#\\#/#;

$workbook || $log_f->logdie( 'xl_converter: NOCATCH: workbook loading Error');

#$debug && print 'saving pdf file: (' . $pdf_output_file . ")\n";
$log_f->debug( 'xl_converter: saving pdf file: (' . $pdf_output_file . ')');

# (-e $xl_output_dir) || 
 # die "\noutput directory does not exist: (" . $pdf_output_file . ")\n";

$pdf_output_file =~ tr#/#\\#;
$workbook->ExportAsFixedFormat( {
			Type => xlTypePDF, 
			Filename => $pdf_output_file, 
			Quality => xlQualityMinimum, 
			IncludeDocProperties => 'False', 
			IgnorePrintAreas => "True", 
			OpenAfterPublish => "False"
		} );
$workbook->Close;
$pdf_output_file =~ tr#\\#/#;
$log_f->info( 'xl_converter: (' . $pdf_output_file . ') export completed');


# move the input file to the output directory
rename($input_file, $xl_output_file) || die "$0: unable to move " . $input_file . " to " . $xl_output_file;
$log_f->info( 'xl_converter: (' . $xl_output_file . ') processing completed');

if ($debug) {
	use File::Copy;
	$log_f->debug( 'xl_converter: copy source (' . $xl_output_file . ') to ' . $input_file);
	copy ($xl_output_file, $input_file);
}	

#$xls->Exit;


sub get_excel {
    my $xls;

    try {
        $xls = Win32::OLE->GetActiveObject('Excel.Application');
		if($xls) {
			while(($xls) && (defined ($xls->Activeworkbook))) { #it is in use
				sleep(1);
			}
		}
    }
    catch {
        die $_;
    };

    unless ( $xls ) {
        $xls = Win32::OLE->new('Excel.Application',  sub { $_[0]->Quit }
        ) or (
			$log_f->logcroak('Cannot start Excel: %s', Win32::OLE->LastError)
		);
    }

	#$debug && (print "get_excel: \$xls = $xls\n");
	$log_f->debug( "xl_converter: get_excel: $xls = (" . $xls . ")");

	$xls->{DisplayAlerts}=0; 
    return $xls;
}