use strict;
use warnings;

use DBI;

use Log::Log4perl qw(get_logger);
use Log::Log4perl;

$ENV{DBI_DRIVER} = 'mysql';

my($internal_host,  $external_host, $db, $host);
my($un, $pw);

$db = '700144_slidefish';
$internal_host = '10.181.5.221';
$external_host = '166.78.8.197';
$host = $external_host;
#$host = $internal_host;
$un = 'windows01';
$pw = 'z0vyrDhgZtwGz6ClT3Do';
    
my $conf_f = q(
log4perl.category.slidefish_f        = INFO, Logfile
log4perl.appender.Logfile          = Log::Log4perl::Appender::File
log4perl.appender.Logfile.filename = C:\slidefish\log\windows_processing.log
log4perl.appender.Logfile.layout = \
	Log::Log4perl::Layout::PatternLayout
log4perl.appender.Logfile.layout.ConversionPattern = %d %F{1} %L> %m %n
);

my $conf_db = q(
	log4perl.category.slidefish_db    = INFO, DB
	log4perl.appender.DB          	= Log::log4perl::Appender::DBI
	log4perl.appender.DB.username		= windows01
	log4perl.appender.DB.password		= z0vyrDhgZtwGz6ClT3Do
	log4perl.appender.DB.sql			= INSERT INTO log VALUES ('%d','%p','%M','%m')
	log4perl.appender.DB.layout		= Log::Log4perl::Layout::PatternLayout 
);

Log::Log4perl::init(\$conf_f);

Log::Log4perl::init(\$conf_db);

my $log_f = get_logger("slidefish_f");
$log_f->error("Blah");
$log_f->info('program started, yay!');

my $dbh = DBI->connect('DBI:mysql:' . $db . ';host=' . $host, $un, $pw);

if($dbh){
	$log_f->info('connected to mysql logging database at address ' . $host);
}
else {
	$log_f->logdie('Failed to connect to mysql database at address ' . $host);
	#die('Failed to connect to mysql database at address ' . $host);
}
	   
my $sth = $dbh->prepare('SELECT * FROM file_processing');
my $results = $dbh->selectall_hashref($sth, 'id');
foreach my $id (keys %$results) {
	print "Value of ID $id is $results->{$id}->{times}\n";
}

$dbh->disconnect();