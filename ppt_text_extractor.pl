#
# C:\Users\stephen\Documents\slidefish\ppt_processor>ppt_text_extractor.pl --test --debug
# --indir C:\Users\stephen\Documents\slidefish\ppt_processor\ppt_documents 
# --outdir C:\Users\stephen\Documents\slidefish\ppt_processor\ppt_output
# --infilen Title1.ppt
#  --textf title1.txt
# --pdf title1.pdf 

use strict; use warnings;
use Try::Tiny;
use Getopt::Long;

require './perl_DB/db_log_DBI.pl';

my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->debug('ppt_text_extractor Logging initialised, yay!');
$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL

use Win32::OLE qw(in with);
use Win32::OLE::Const qw( Microsoft.PowerPoint );
use Win32::OLE::Const "Microsoft Office 15.0 Object Library";  # mso constants

use Win32::OLE::Enum;
$Win32::OLE::Warn = 3;

my $ppc = Win32::OLE::Const->Load(qw( Microsoft.PowerPoint ));

my ($ppt_input_dir, $ppt_output_dir, $ppt_output_file, $test, $debug);
my ($input_fn, $out_txt_fn, $out_pdf_fn);
my ($input_file, $text_output_file, $pdf_output_file);
$ppt_input_dir = $ppt_output_dir = $input_fn = '';
$ppt_output_file = '';
$out_txt_fn = $out_pdf_fn = '';
$input_file = $text_output_file = $pdf_output_file = '';
$test = $debug = 0;


if($test) {

	# $log_f->debug('ppt_text_extractor: using test variables');
	$ppt_input_dir = qw( C:/slidefish/processing );  
	$ppt_output_dir = qw( C:/slidefish/output ); 
	$input_fn = "perl2000.ppt";
	$out_txt_fn = "perl2000.ppt.txt";
	$out_pdf_fn = "perl2000.ppt.pdf";
	
 }
 
  GetOptions (
			'test!' => \$test,
			'debug!' => \$debug,
			'indir=s' => \$ppt_input_dir,
			'outdir=s' => \$ppt_output_dir,
			'infilen=s' => \$input_fn,
			'textf=s' => \$out_txt_fn,
			'pdf=s' => \$out_pdf_fn);
			
$ppt_input_dir =~ tr#\\#/#;
$ppt_output_dir =~ tr#\\#/#;
$input_file = $ppt_input_dir . "/" . $input_fn;
$text_output_file = $ppt_output_dir . "/" . $out_txt_fn;
$pdf_output_file = $ppt_output_dir . "/" . $out_pdf_fn;
$ppt_output_file = $ppt_output_dir . "/" . $input_fn;

$debug && $log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL

$log_f->debug( "ppt_text_extractor: inputdir: (" . $ppt_input_dir . "), ouputdir: (" . $ppt_output_dir  . ")");

(-e $ppt_input_dir) || 
	$log_f->logdie("INPUT DIRECTORY FAILURE: $ppt_input_dir!");
(-e $ppt_output_dir) || 
	$log_f->logdie("OUTPUT DIRECTORY FAILURE: $ppt_output_dir!");

$log_f->debug('ppt_text_extractor: Input file: (' . $input_file  . ')');
$log_f->debug('ppt_text_extractor: text output file: (' . $text_output_file . ')');

my($ppt, $presentation, $slides, $slide, @slide_notes, $i_slide);
$ppt = get_ppt();
#$ppt->{Visible} = 1;
$test && ($ppt->{Visible} = 1);

# $input_file =~ tr#/#\\#;
$log_f->debug('ppt_text_extractor: opening Input file: (' . $input_file . ')');
$log_f->debug('ppt_text_extractor: length Input file name: (' . length($input_file) . ')');

(-e $ppt_output_dir) || 
 die "\ninput file does not exist: (" . $input_file . ")\n";
 
 # open the presentation
(-e $input_file) ||
	$log_f->logcroak(sprintf("ppt_text_extractor: input file (%s) does not exist", $input_file));
 $presentation = $ppt->Presentations->Open($input_file, 1, 1, 0);

$presentation || $log_f->logdie( 'ppt_text_extractor: NOCATCH: presentation loading Error');
$slides = Win32::OLE::Enum->new( $presentation->Slides );

@slide_notes = '';
$i_slide = 0;

SLIDE:
# extract text from notes section
while ( $slide = $slides->Next ) {

	$i_slide++;
	$log_f->debug('ppt_text_extractor: Start of slide: ' . $i_slide);
	$log_f->debug( 'ppt_text_extractor: ' . sprintf("%s:\t Title shapes:\t ", $slide->Name) . ')');
	
    my $shapes = Win32::OLE::Enum->new( $slide->Shapes );
    TITLE:
    while ($debug && ( my $shape = $shapes->Next )) {
		if( $shape->Type == msoPlaceholder) {
			my $type = $shape->PlaceholderFormat->Type;
			if ( $type == ppPlaceholderTitle
					or $type == ppPlaceholderCenterTitle
					or $type == ppPlaceholderVerticalTitle
			) {
				$log_f->debug( 'ppt_text_extractor: slide' . sprintf("%d:\t %s", $i_slide, $shape->TextFrame->TextRange->text) . ')');
			}
		}
    }

    SUBTITLE:
    #$debug && printf "%s:\t Subtitle:\t", $slide->Name;
	$log_f->debug('ppt_text_extractor: slide' . sprintf(" %d:\t Subtitle shapes:\t", $i_slide, $slide->Name) . ')');
	$shapes->Reset();
    while ($debug && ( my $shape = $shapes->Next )) {
 		if( $shape->Type == msoPlaceholder) {
		    my $type = $shape->PlaceholderFormat->Type;
			if ( $type == ppPlaceholderSubtitle
					or $type == ppPlaceholderBody
					or $type == ppPlaceholderVerticalBody
			) {
				$log_f->debug( 'ppt_text_extractor: Text: (' . $shape->TextFrame->TextRange->text . ')');
			}
		}
    }

	NOTES:

	$log_f->debug( 'ppt_text_extractor: Notes shapes:)');
	
	$shapes = Win32::OLE::Enum->new( $slide->NotesPage->Shapes );
	my $i_shape = 0;
	while ( my $shape = $shapes->Next ) {
        my $type = $shape->PlaceholderFormat->Type;

		$log_f->debug( 'ppt_text_extractor: ' . sprintf("Shape%03d\n", $i_shape));
		
        if (($type == ppPlaceholderBody) && (length($shape->TextFrame->TextRange->text))) {

			$log_f->debug( 'ppt_text_extractor: (' . sprintf("Slide%03d: %s\n", $i_slide, $shape->TextFrame->TextRange->text) . ')');
			$slide_notes[$#slide_notes + 1] = sprintf("Slide%03d: %s\n", $i_slide, $shape->TextFrame->TextRange->text);
        }
	}

	#$debug && print "notes->PlaceholderFormat->type: " . $slide->NotesPage->Shapes->Placeholders(2)->PlaceholderFormat->Type . "\n";
	$log_f->debug( 'ppt_text_extractor: Last Error: (' . Win32::OLE->LastError() . ')');
	
    # $debug && print "End of slide $i_slide\n";
	$log_f->debug( 'ppt_text_extractor: Last Error: (' . Win32::OLE->LastError() . ')');
}

# save notes to text file
$log_f->info( 'ppt_text_extractor: saving text file: (' . $text_output_file . ')');

open my $fh, '>', $text_output_file or die "Cannot open output.txt: $!";
print $fh join('', @slide_notes[0..$#slide_notes]); # Print each entry in our array to the file
close $fh; # Not necessary, but nice to do

$log_f->debug( 'ppt_text_extractor: All notes: (' . join(": ", @slide_notes[0..$#slide_notes]) . ')');

#$debug && print 'saving pdf file: (' . $pdf_output_file . ")\n";
$log_f->debug( 'ppt_text_extractor: saving pdf file: (' . $pdf_output_file . ')');

# (-e $ppt_output_dir) || 
 # die "\noutput directory does not exist: (" . $pdf_output_file . ")\n";

$pdf_output_file =~ tr#/#\\#;
$presentation->SaveAs($pdf_output_file, ppSaveAsPDF, 1);
$presentation->Close;
$pdf_output_file =~ tr#\\#/#;
$log_f->info( 'ppt_text_extractor: (' . $pdf_output_file . ') export completed');


# move the input file to the output directory
rename($input_file, $ppt_output_file) || die "$0: unable to move " . $input_file . " to " . $ppt_output_file;
$log_f->info( 'ppt_text_extractor: (' . $ppt_output_file . ') processing completed');

if ($debug) {
	use File::Copy;
	$log_f->debug( 'ppt_text_extractor: copy source (' . $ppt_output_file . ') to ' . $input_file);
	copy ($ppt_output_file, $input_file);
}	

#$ppt->Exit;


sub get_ppt {
    my $ppt;

    try {
        $ppt = Win32::OLE->GetActiveObject('PowerPoint.Application');
		if($ppt) {
			while(($ppt) && (defined ($ppt->ActivePresentation))) { #it is in use
				sleep(1);
			}
		}
    }
    catch {
        die $_;
    };

    unless ( $ppt ) {
        $ppt = Win32::OLE->new('PowerPoint.Application',  sub { $_[0]->Quit }
        ) or sub {
			die sprintf(
				'Cannot start PowerPoint: %s', Win32::OLE->LastError
			);
		}
    }

	#$debug && (print "get_ppt: ppt = $ppt\n");
	$log_f->debug( "ppt_text_extractor: get_ppt: ppt = (" . $ppt . ")");

    return $ppt;
}